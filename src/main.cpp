
#include "Gestor.h"

int main(int argc, char **argv)
{
	Gestor1 *gestor;
	
	if (argc == 2 && argv[1][0] == '2')
		gestor = new Gestor2();
	else if (argc == 2 && argv[1][0] == '3')
		gestor = new Gestor3();
	else
		gestor = new Gestor1();

	gestor->MainLoop();
	
	cout << endl;
	return 0;
}
