/**
 * configuraciones interesantes:
 * arte cubista: resistencia = 7, valor 3 en la cruz y 2 en las esquinas
 * naves azules: resistencia[] = {1, 1, 2}, valor = 1
 * piedra papel tijera: resistencia = 2, valor = 1
 * coral: resistencia = {2, 6, 6} con valor 3 y 2
 */
#include "Gestor.h"
Gestor1::Gestor1()//:Ventana(20, 20)
{
	int i;
	srand(time(NULL));

	edad_inicial = 30;
	
	atribs[0] = {NEGRO, 0, 0, 0};
	//atribs[1] = {ROJO, 3, 4, edad_inicial};
	//atribs[2] = {VERDE, 1, 4, edad_inicial};
	//atribs[3] = {AZUL, 2, 6, edad_inicial};
	
	atribs[1] = {ROJO, 3, 4, edad_inicial};
	atribs[2] = {VERDE, 1, 6, edad_inicial};
	atribs[3] = {AZUL, 2, 6, edad_inicial};
	
	w = screen->w;
	h = screen->h;
	pcx = pcy = 2;
	cx = w/pcx;
	cy = h/pcy;
	mapa = new Presa[cx*cy];
	cout << "g1: cx*cy = " << cx*cy << endl;	// DEPURACION
	mapaux = new Presa[cx*cy];
	nhilos = 4;
	usar_hilos = 3;

	hilos = new SDL_Thread*[nhilos];
	datos_hilos = new DatosHilos[nhilos];
	mapahaux = new Presa*[nhilos];
	for (i = 0; i < nhilos; i++)
		mapahaux[i] = new Presa[cx*cy/nhilos];
	
	pixel = false;
	pincel = 0;
	envejecimiento = 1;
	for (i = 0; i < cx*cy; i++)
	{
		mapa[i].idAtr = rand()%3 + 1;	// o verde o rojo o azul, "aleatoriamente"
		mapa[i].edad = edad_inicial;
	}
		//mapaux[i].idAtr = mapa[i].idAtr = rand()%3 + 1;	// o verde o rojo o azul, "aleatoriamente"

	escribir_mapa = SDL_CreateMutex();
	terminar = SDL_CreateMutex();
	procesar = SDL_CreateSemaphore(0);
	sincronizar = SDL_CreateSemaphore(0);
	terminados = 0;
	if (usar_hilos == 3)
		CrearHilos(&ActualizarEnBucle);
	semd.Sumar();
}

Gestor2::Gestor2(): Gestor1()
{
	edad_inicial = 100;
	bloqueo = 28;

	atribs[1].resistencia = atribs[2].resistencia = atribs[3].resistencia = edad_inicial/2;
}

Gestor1::~Gestor1()
{
	SDL_mutexP(escribir_mapa);
	delete[] mapa;
	delete[] mapaux;
	for (int i = 0; i < nhilos; i++)
		delete mapahaux[i];
	delete[] mapahaux;
	SDL_mutexV(escribir_mapa);
}

void Gestor1::OnStep(float dt)
{
	int x, y, i;
	//Uint32 *intercambio;
	//using namespace Memoria;
	SDL_mutexP(escribir_mapa);
	switch(usar_hilos)
	{
		case 0:	// sin hilos
			for (y = cy; y < 2*cy; y++)
				for (x = cx; x < 2*cx; x++)
					Actualizar(x, y, mapaux);

			memcpy(mapa, mapaux, sizeof(Presa)*cx*cy);
			break;
		case 1:	// hilos de la SDL, creados y destruidos cada vez
			CrearHilos(&ActualizarZona);
			for (i = 0; i < nhilos; i++)
				SDL_WaitThread(hilos[i], NULL);
			for (i = 0; i < nhilos; i++)
				memcpy(mapa + i*cx*cy/nhilos, mapahaux[i], sizeof(Presa)*cx*cy/nhilos);
			break;
		case 2:	// hilos con openmp
#pragma omp parallel for private(x, y)
			for (y = cy; y < 2*cy; y++)
				for (x = cx; x < 2*cx; x++)
					Actualizar(x, y, mapaux);

			memcpy(mapa, mapaux, sizeof(Presa)*cx*cy);
			break;
		case 3:	// hilos de la SDL creados y destruidos una sola vez
			for (i = 0; i < nhilos; i++)	// avisar para que empiecen
				SDL_SemPost(procesar);
			SDL_SemWait(sincronizar);	// cuando hayan terminado de escribir
			for (i = 0; i < nhilos; i++)
				memcpy(mapa + i*cx*cy/nhilos, mapahaux[i], sizeof(Presa)*cx*cy/nhilos);
			break;
	}
	SDL_mutexV(escribir_mapa);
//cout << "dt = " << dt << endl;	// DEPURACION
}

void Gestor1::CrearHilos(int (*f)(void*))
{
	for (int i = 0; i < nhilos; i++)
	{
		datos_hilos[i].g = this;
		datos_hilos[i].i = i;
		getitimer(ITIMER_PROF, &datos_hilos[i].itimer);
		hilos[i] = SDL_CreateThread(f, &datos_hilos[i]);
	}
}

int ActualizarEnBucle(void * datos)
{
	Gestor1 * g = ((DatosHilos*)datos)->g;
	int i;
	cout << "empieza un hilo " << endl;
	setitimer(ITIMER_PROF, &((DatosHilos*)datos)->itimer, NULL);
	SDL_SemWait(g->procesar);
	while (g->usar_hilos == 3)
	{
		//printf ("%s:%d empiezo iteracion bucle\n", __FILE__, __LINE__);	//DEPURACION
		ActualizarZona(datos);
		SDL_mutexP(g->terminar);
		g->terminados++;
		if (g->terminados == g->nhilos)
		{
			g->terminados = 0;
			for (i = 0; i < g->nhilos+1; i++)	// avisar para preparar la siguiente iteracion, +1 para el padre
				SDL_SemPost(g->sincronizar);
		}
		SDL_mutexV(g->terminar);
		SDL_SemWait(g->sincronizar);
		//printf ("%s:%d sincronizados: espero a procesar\n", __FILE__, __LINE__);	//DEPURACION
		SDL_SemWait(g->procesar);
		//printf ("%s:%d  termino iteracion bucle\n", __FILE__, __LINE__);	//DEPURACION
	}
	SDL_SemPost(g->sincronizar);	// uso final: contar cuantos hilos se mueren
	printf ("%s:%d acaba un hilo\n", __FILE__, __LINE__);	//DEPURACION
}

int ActualizarZona(void * datos)
{
	int x, y;
	int i = ((DatosHilos*) datos)->i;
	Gestor1 *g = ((DatosHilos*) datos)->g;

	for (y = g->cy + i*g->cy/g->nhilos; y < g->cy + (i+1)*g->cy/g->nhilos; y++)
		for (x = g->cx; x < 2*g->cx; x++)
			g->Actualizar(x, y, g->mapahaux[i] - g->cx*i*g->cy/g->nhilos);
}
/**
 * escribe en mapaux la iteracion siguiente de mapa
 */
void Gestor1::Actualizar(int x, int y, Presa * mapaux)
{
	int i, j;
	int yo;	// mi id
	int el;	// el id del que me rodea
	int cazadores[N_ATR];
	int maximo;
	int elem(x%cx + cx*(y%cy));
	//cout << "x = " << x << endl;	// DEPURACION
	//cout << "y = " << y << endl;	// DEPURACION
	//cout << "x%cx = " << x%cx << endl;	// DEPURACION
	//cout << "y%cy = " << y%cy << endl;	// DEPURACION
	//cout << "elem = " << elem << endl;	// DEPURACION
	

	memset (cazadores, 0, N_ATR*sizeof (int));
	
	yo = mapa[elem].idAtr;
	
	for (i = -1; i < 2; i++)
		for (j = -1; j < 2; j++)	// contar lo que hay alrededor 
		{
			el = mapa[(x+i)%cx + cx*((y+j)%cy)].idAtr;
			cazadores[el] += i*j? 2: 3;	// i*j==0 es solo la cruz, en vez de los ocho de alrededor
		}

	if (yo == 0)
	{
		maximo = Maximo(cazadores[1], cazadores[2], cazadores[3]) + 1;
		if (cazadores[maximo] > atribs[0].resistencia)	// se comen una negra
		{
			mapaux[elem].idAtr = maximo;
			mapaux[elem].edad = edad_inicial;
		}
		else
		{
			mapaux[elem].idAtr = 0;
			mapaux[elem].edad = 0;
		}
	}
	else
		if (cazadores[atribs[yo].depredador] > atribs[yo].resistencia)	// si tengo alrededor mas cazadores de los que puedo aguantar
		{
			mapaux[elem].idAtr = atribs[yo].depredador;	// me comen y me sustituyen por una cria de ellos
			mapaux[elem].edad = edad_inicial;
			
		}
		else	// si resisto
		{
			if (mapa[elem].edad)	// aun me queda edad
			{
				mapaux[elem].idAtr = yo;
				if (mapa[elem].edad < envejecimiento)
					mapaux[elem].edad = 0;
				else
					mapaux[elem].edad = mapa[elem].edad - envejecimiento;
			}
			else
				mapaux[elem].idAtr = 0;
		}
}

/**
 * version alternativa para hacer "neuronas"
 */
void Gestor2::Actualizar(int x, int y, Presa * mapaux)
{
	int i, j;
	int yo;	// mi id
	Presa el;	// el id del que me rodea
	int energia[N_ATR];
	int mayor[N_ATR];
	int cuantos[N_ATR];
	int maximo;
	int elem(x%cx + cx*(y%cy));
	Uint8 edadaux;
	//cout << "x = " << x << endl;	// DEPURACION
	//cout << "y = " << y << endl;	// DEPURACION
	//cout << "x%cx = " << x%cx << endl;	// DEPURACION
	//cout << "y%cy = " << y%cy << endl;	// DEPURACION
	//cout << "elem = " << elem << endl;	// DEPURACION

	memset (energia, 0, N_ATR*sizeof (int));
	memset (mayor, 0, N_ATR*sizeof (int));
	memset (cuantos, 0, N_ATR*sizeof (int));

	yo = mapa[elem].idAtr;

	for (i = -1; i < 2; i++)
		for (j = -1; j < 2; j++)	// contar lo que hay alrededor 
		{
			el = mapa[(x+i)%cx + cx*((y+j)%cy)];
			cuantos[el.idAtr]++;
			mayor[el.idAtr] = el.edad -1 > mayor[el.idAtr]? el.edad-1: mayor[el.idAtr];	// max(energia, el.edad)
			energia[el.idAtr] += el.edad-1;	// i*j==0 es solo la cruz, en vez de los ocho de alrededor
		}

	if (yo == 0)
	{
		mapaux[elem].idAtr = 0;
		mapaux[elem].edad = 0;
	}
	else
	{
		mapaux[elem].idAtr = yo;
		edadaux = mapa[elem].edad;

		if (edadaux == 1)
		{
			if (mayor[atribs[yo].depredador] > atribs[yo].resistencia)	// si tengo alrededor mas energia de la que puedo aguantar
				edadaux = edad_inicial;
			else if (mayor[yo] >= bloqueo)	// si estoy preparado y alguien me activa
				edadaux = mayor[yo]+1;
		}
		else if (edadaux > bloqueo)	// si estoy activo me bloqueo
			edadaux = bloqueo;
		else	// envejezco
		{
			if (edadaux <= envejecimiento)
				edadaux = 1;
			else
				edadaux = edadaux - envejecimiento;
		}

		mapaux[elem].edad = edadaux;
			
		

			

		/*
		mapaux[elem].idAtr = yo;
		edadaux = mapa[elem].edad;
		if (mayor[yo] > edadaux)// + bloqueo/)
			edadaux = mayor[yo];
		
		if (edadaux <= envejecimiento)
			edadaux = 1;
		else
			edadaux = edadaux - envejecimiento;
		if (mayor[atribs[yo].depredador] > atribs[yo].resistencia)	// si tengo alrededor mas energia de la que puedo aguantar
		{
			if (yo == 3i)	// && edad_inicial - edadaux > bloqueo/)	// si soy el axon y no acabo de disparar
				edadaux = edad_inicial;//mayor[atribs[yo].depredador];
			else if (yo == 2)
			{
				maximo = energia[atribs[yo].depredador] < edad_inicial? energia[atribs[yo].depredador]: edad_inicial;	// min
				edadaux = maximo > edadaux? maximo: edadaux;
			}
			else
			{
				maximo = mayor[atribs[yo].depredador] < edad_inicial? mayor[atribs[yo].depredador]: edad_inicial;	// min
				edadaux = maximo > edadaux? maximo: edadaux;
			}
		}
		mapaux[elem].edad = edadaux;

		if (edadaux > 100)
		{
			cout << "no mola: edadaux = " << int(edadaux) << endl;	// DEPURACION
			cout << "x = " << x << endl;	// DEPURACION
			cout << "y = " << y << endl;	// DEPURACION
		}
		// */
		/*
		   if (energia[atribs[yo].depredador] > atribs[yo].resistencia)	// si tengo alrededor mas energia de la que puedo aguantar
		   {
		//mapaux[elem].edad += energia[atribs[yo].depredador];
		}

		if (mapa[elem].edad > 1)	// aun me queda edad
		{
		if (mapa[elem].edad < envejecimiento)
		mapaux[elem].edad = 0;
		else
		mapaux[elem].edad = mapa[elem].edad - envejecimiento;
		}
		mapaux[elem].idAtr = yo;
		if (mapa[elem].edad <= 1 + envejecimiento)
		mapaux[elem].edad = mayor[yo] > mapa[elem].edad? mayor[yo]: mapa[elem].edad;	// max
		 */
	}
}


/**
 * procesa lo que hay en mapa y lo vuelca en la screen
 */
void Gestor1::OnDisplay()
{
	Uint16 x, y;
	Uint32 color;

	//cout << "on display: mapa[0] = " << mapa[0] << endl;	// DEPURACION

	SDL_mutexP(escribir_mapa);
	SDL_FillRect(screen, NULL, NEGRO);

	//if (pixel)
	//{
	//for (y = 0; y < cy; y++)
	//for (x = 0; x < cx; x++)
	//{
	////scraux = 
	////}
	////scraux = SDL_CreateRGBSurfaceFrom (mapa, screen->w, screen->h, screen->format->BitsPerPixel, screen->pitch, 0, 0, 0, 0);
	////SDL_BlitSurface(scraux, NULL, screen, NULL);
	//}
	//else
	//{
	for (y = 0; y < cy; y++)
		for (x = 0; x < cx; x++)
		{
			color = atribs[mapa[x + y*cx].idAtr].color;
			PintarRect(x*pcx, y*pcy, pcx, pcy, (color*mapa[x + y*cx].edad/edad_inicial)&color);
		}
	//}

	SDL_Flip(screen);
	SDL_mutexV(escribir_mapa);
}

void Gestor1::OnKeyPressed(SDL_Event &e)
{
	int entero, i;
	switch(e.key.keysym.sym)
	{
		case SDLK_SPACE:
			sems.Sumar();
			break;
		case SDLK_p:	// play / stop
			if (semd.Estado())
			{
				cout << "stop" << endl;
				sems.Cerrar();
				semd.Cerrar();
			}
			else
			{
				cout << "play" << endl;
				sems.Abrir();
				semd.Abrir();
			}
			break;
		case SDLK_w:	// cambiar resolucion de la Window 
			cout << "resolucion actual (" << w << ", " << h << "), nueva:" << endl;
			SDLcin(w);
			SDLcin(h);
			OnResize(w, h);
			break;
		case SDLK_r:
			pincel = 1;
			break;
		case SDLK_g:
			pincel = 2;
			break;
		case SDLK_b:
			pincel = 3;
			break;
		case SDLK_n:
			pincel = 0;
			break;
		case SDLK_j:
			SDL_mutexP(escribir_mapa);
			for (i = 0; i < cx*cy; i++)
			{
				mapa[i].idAtr = 3;
				mapa[i].edad = rand()%255;
			}
			SDL_mutexV(escribir_mapa);
			break;

		case SDLK_k:
			SDL_mutexP(escribir_mapa);
			for (i = 0; i < cx*cy; i++)
			{
				mapa[i].idAtr = rand()%3 + 1;	// o verde o rojo o azul, "aleatoriamente"
				mapa[i].edad = edad_inicial;
			}
			SDL_mutexV(escribir_mapa);
			break;
		case SDLK_l:
			SDL_mutexP(escribir_mapa);
			memset(mapa, 0, cx*cy*sizeof(Presa));
			SDL_mutexV(escribir_mapa);
			break;
		case SDLK_m:
			if (usar_hilos == 3)
			{
				usar_hilos = 4;	// para forzar a los hilos a salir del bucle
				for (i = 0; i < nhilos; i++)	// avisar para que empiecen
					SDL_SemPost(procesar);
				printf ("%s:%d empiezo a esperar\n ", __FILE__, __LINE__);	//DEPURACION
				for (i = 0; i < nhilos; i++)	// avisar para que empiecen
					SDL_SemWait(sincronizar);	// cuando hayan terminado de escribir
				printf ("%s:%d termino de esperar\n ", __FILE__, __LINE__);	//DEPURACION
				usar_hilos = 3;
			}
			cout << "SDL_SemValue(sincronizar) = " << SDL_SemValue(sincronizar) << endl;	// DEPURACION
			cout << "SDL_SemValue(procesar) = " << SDL_SemValue(procesar) << endl;	// DEPURACION
			cout << "terminados = " << terminados << endl;	// DEPURACION

			cout << "tipo de hilos usados (sin hilos, SDL_Thread, openmp, SDL_Thread permanentes) actualmente: " << usar_hilos << ", nuevo:" << endl;
			SDLcin(usar_hilos);
			cout << "usar_hilos = " << usar_hilos << endl;	// DEPURACION
			if (usar_hilos == 3)
				CrearHilos(&ActualizarEnBucle);
			break;
		case SDLK_a:
			CambiarAtribs();
			break;
		case SDLK_v:
			cout << "envejecimiento actual " << envejecimiento << ", nuevo:" << endl;
			//cin >> envejecimiento;
			SDLcin(envejecimiento);
			break;
		case SDLK_e:
			cout << "edad inicial actual " << edad_inicial << ", nueva:" << endl;
			SDLcin(edad_inicial);
			break;
		case SDLK_c:	// tamanyo de los pixeles
			cout << "pixels por cuadro actuales " << pcy << ", nuevos:" << endl;
			SDLcin(entero);
			SDL_mutexP(escribir_mapa);
			pcx = pcy = entero;
			OnResize((w/pcx)*pcx, (h/pcy)*pcy);
			SDL_mutexP(escribir_mapa);
			break;
		case SDLK_h:
			MostrarAyuda();
			break;
		case SDLK_f:
			cout << "Frames por segundo actuales " << 1000.0/mspf << ", nuevos:" << endl;
			SDLcin(entero);
			setFps(entero);
			break;
		default:
			break;
	}
	semd.Sumar();
}

void Gestor1::OnMouseButton(SDL_Event &e)
{
	//cout << "pincel = " << pincel << endl;	// DEPURACION
	//cout << "mapa[e.button.x/pcx + e.button.y/pcy*cx] = " << mapa[e.button.x/pcx + e.button.y/pcy*cx] << endl;	// DEPURACION

	if (e.button.button == SDL_BUTTON_LEFT)
	{
		SDL_mutexP(escribir_mapa);
		//if (pixel)
		//mapa[e.button.x + e.button.y*cx].idAtr = pincel;
		//else
		//{
		mapa[e.button.x/pcx + e.button.y/pcy*cx].idAtr = pincel;
		mapa[e.button.x/pcx + e.button.y/pcy*cx].edad = edad_inicial;
		//}
		SDL_mutexV(escribir_mapa);
		//cout << "mapa[e.button.x/pcx + e.button.y/pcy*cx] = " << mapa[e.button.x/pcx + e.button.y/pcy*cx] << endl;	// DEPURACION
		semd.Sumar();
		//cout << "e.state = " << e.button.state << endl;	// DEPURACION
	}
	else
	{
		cout << "e.button.x = " << e.button.x << endl;	// DEPURACION
		cout << "e.button.y = " << e.button.y << endl;	// DEPURACION
		cout << "mapa[e.button.x/pcx + e.button.y/pcy*cx].idAtr = " << (int)mapa[e.button.x/pcx + e.button.y/pcy*cx].idAtr << endl;	// DEPURACION
		cout << "mapa[e.button.x/pcx + e.button.y/pcy*cx].edad = " << (int)mapa[e.button.x/pcx + e.button.y/pcy*cx].edad << endl;	// DEPURACION
		
	}
}

void Gestor1::OnMouseMotion(SDL_Event &e)
{
	if (e.motion.state & SDL_BUTTON(1))
		OnMouseButton(e);
}

void Gestor1::OnResize(int w_p, int h_p)
{
	int i;
	SDL_mutexP(escribir_mapa);
	delete[] mapa;
	delete[] mapaux;
	for (i = 0; i < nhilos; i++)
		delete mapahaux[i];

	w = w_p;
	h = h_p;
	cx = w/pcx;
	cy = h/(pcy*nhilos)*nhilos;

	mapa = new Presa[cx*cy];
	cout << "cx*cy = " << cx*cy << endl;	// DEPURACION
	mapaux = new Presa[cx*cy];
	for (i = 0; i < nhilos; i++)
		mapahaux[i] = new Presa[cx*cy/nhilos];

	for (int i = 0; i < cx*cy; i++)
	{
		mapa[i].idAtr = rand()%3 + 1;	// o verde o rojo o azul, "aleatoriamente"
		mapa[i].edad = edad_inicial;
	}
	SDL_mutexV(escribir_mapa);
}

void Gestor1::MostrarAyuda()
{
	cout << "Accion de las teclas: \n\n";

	cout << "SPACE: avanzar un paso" << endl;
	cout << "p: play/stop, avanzar indefinidamente" << endl;
	cout << "w: cambiar resolucion de la ventana en cuadros" << endl;
	cout << "r: pintar en rojo" << endl;
	cout << "g: pintar en verde" << endl;
	cout << "b: pintar en azul" << endl;
	cout << "n: pintar en negro" << endl;
	cout << "k: rellenar aleatoriamente" << endl;
	cout << "l: limpiar, poner en negro todo" << endl;
	cout << "m: modo multihilo" << endl;
	cout << "a: cambiar atributos de las presas" << endl;
	cout << "v: cambiar envejecimiento" << endl;
	cout << "e: cambiar edad inicial" << endl;
	cout << "c: cambiar pixels por cuadro" << endl;
	cout << "h: mostrar esta ayuda" << endl;
	cout << endl;
}


void Gestor1::CambiarAtribs()
{
	int tipo = 0;
	cout << "configuracion actual: " << endl;
	cout << "	ROJO res: " << atribs[1].resistencia << endl;
	cout << "	VERDE res: " << atribs[2].resistencia << endl;
	cout << "	AZUL res: " << atribs[3].resistencia << endl;
	cout << "\ntipo 1 (coral): [4, 6, 6]" << endl; 
	cout << "tipo 2 (ppt): [6, 6, 6]" << endl; 
	cout << "tipo 3 (naves): [3, 3, 6]" << endl; 
	cout << "tipo 4 (cubismo): [7, 7, 7]" << endl; 
	cout << "tipo elegido (0 para poner otro):" << endl;
	SDLcin(tipo);
	switch (tipo)
	{
		case 0:
			SDLcin(atribs[1].resistencia);
			SDLcin(atribs[2].resistencia);
			SDLcin(atribs[3].resistencia);
			break;
		case 1:
			atribs[1].resistencia = 4;
			atribs[2].resistencia = 6;
			atribs[3].resistencia = 6;
			break;
		case 2:
			atribs[1].resistencia = 6;
			atribs[2].resistencia = 6;
			atribs[3].resistencia = 6;
			break;
		case 3:
			atribs[1].resistencia = 3;
			atribs[2].resistencia = 3;
			atribs[3].resistencia = 6;
			break;
		case 4:
			atribs[1].resistencia = 7;
			atribs[2].resistencia = 7;
			atribs[3].resistencia = 7;
			break;
	}
	cout << "configuracion actual: " << endl;
	cout << "	ROJO res: " << atribs[1].resistencia << endl;
	cout << "	VERDE res: " << atribs[2].resistencia << endl;
	cout << "	AZUL res: " << atribs[3].resistencia << endl;
}


Gestor3::Gestor3()
{
	int i;
	srand(time(NULL));

	edad_inicial = 255;
	
	atribs[0] = {0x0, 0, 0, edad_inicial};
	//atribs[1] = {ROJO, 3, 4, edad_inicial};
	//atribs[2] = {VERDE, 1, 4, edad_inicial};
	atribs[3] = {AZUL, 0, 0, edad_inicial};
	
	//atribs[1] = {ROJO, 3, 4, edad_inicial};
	//atribs[2] = {VERDE, 1, 6, edad_inicial};
	//atribs[3] = {AZUL, 2, 6, edad_inicial};
	
	pcx = pcy = 2;
	usar_hilos = 0;

	pixel = false;
	pincel = 3;
	for (i = 0; i < cx*cy; i++)
	{
		mapa[i].idAtr = 3;
		mapa[i].edad = rand()%255;
	}

	semd.Sumar();
}

void Gestor3::Actualizar(int x, int y, Presa * mapaux)
{
	int i, j;
	int media = 0;
	int elem(x%cx + cx*(y%cy));
	bool ponderado = true;
	int coef;
	int num_vecinos = ponderado? 23: 9;

	for (i = -1; i < 2; i++)
		for (j = -1; j < 2; j++)	// contar lo que hay alrededor 
		{
			if (ponderado)
				if (i*j == 0)
					coef = 3;
				else
					coef = 2;
			else
				coef = 1;

			media += mapa[(x+i)%cx + cx*((y+j)%cy)].edad * coef;
		}

	media = (media + num_vecinos/2)/num_vecinos;
	mapaux[elem].idAtr = 3;
	mapaux[elem].edad = media;
}

void Gestor3::OnMouseButton(SDL_Event &e)
{
	
	if (e.button.button == SDL_BUTTON_LEFT)
	{
		SDL_mutexP(escribir_mapa);
		
		mapa[e.button.x/pcx + e.button.y/pcy*cx].idAtr = pincel;
		mapa[e.button.x/pcx + e.button.y/pcy*cx].edad = pincel==0? 0: edad_inicial;
		
		SDL_mutexV(escribir_mapa);
		semd.Sumar();
	}
	else
	{
		cout << "e.button.x = " << e.button.x << endl;	// DEPURACION
		cout << "e.button.y = " << e.button.y << endl;	// DEPURACION
		cout << "mapa[e.button.x/pcx + e.button.y/pcy*cx].idAtr = " << (int)mapa[e.button.x/pcx + e.button.y/pcy*cx].idAtr << endl;	// DEPURACION
		cout << "mapa[e.button.x/pcx + e.button.y/pcy*cx].edad = " << (int)mapa[e.button.x/pcx + e.button.y/pcy*cx].edad << endl;	// DEPURACION
		
	}
}
