#ifndef _MEMORIA_H_
#define _MEMORIA_H_

#include <SDL.h>
#include <iostream>
using namespace std;
#include "Ventana.h"



int Maximo(int &a, int &b, int &c);
void CambiarAtribs();

struct Atributos
{
	Uint32 color;
	int depredador;
	int resistencia;
	int edad_max;
};

struct Presa
{
	Uint8 idAtr;
	Uint8 edad;
};

const int N_ATR = 4;

const Uint32 NEGRO = 0x505050;
//const Uint32 ROJO = 0xad1818;
//const Uint32 VERDE = 0x000001;
//const Uint32 AZUL = 0xffffff;
const Uint32 ROJO = 0xff0000;	//SDL_MapRGB(screen->format, 255, 0, 0);
const Uint32 VERDE = 0xff00;	//SDL_MapRGB(screen->format, 0, 255, 0);
const Uint32 AZUL = 0xff;	//SDL_MapRGB(screen->format, 0, 0, 255);
struct Memoria
{
	SDL_Surface *scraux;
	bool pixel;
	Uint16 cx, cy;
	Uint16 pcx, pcy;
	int w, h;
	Uint32 pincel;
	Atributos atribs[N_ATR];
	Presa *mapa, *mapaux, **mapahaux;
	int edad_inicial;
	int envejecimiento;
	int bloqueo;
};

#endif
