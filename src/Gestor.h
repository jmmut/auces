#ifndef _GESTOR_H_
#define _GESTOR_H_

#include <omp.h>
#include <sys/time.h>
#include "Ventana.h"
#include "Memoria.h"

class Gestor1;
typedef struct
{
	Gestor1 *g;
	int i;
	struct itimerval itimer;
} DatosHilos;

class Gestor1: public Ventana, public Memoria
{
	public:
		Gestor1();
		~Gestor1();
		void OnStep(float);
		void OnDisplay();
		void OnKeyPressed(SDL_Event &e);
		void OnMouseButton(SDL_Event &e);
		void OnMouseMotion(SDL_Event &e);
		void OnResize(int w, int h);

		void CrearHilos(int (*f)(void*));
		virtual void Actualizar(int x, int y, Presa*);
		void MostrarAyuda();
		void CambiarAtribs();

		int nhilos;
		int usar_hilos;
		SDL_Thread **hilos;
		DatosHilos *datos_hilos;
		int terminados;

		SDL_mutex *escribir_mapa, *terminar;
		SDL_sem *procesar, *sincronizar;
};

class Gestor2: public Gestor1
{
	public:
		Gestor2();
		void Actualizar(int x, int y, Presa*);
};

class Gestor3: public Gestor1
{
	public:
		Gestor3();
		void Actualizar(int x, int y, Presa*);
		void OnMouseButton(SDL_Event &e);
};

int ActualizarEnBucle(void * datos);
int ActualizarZona(void * datos);

#endif
